#!/usr/bin/python
import os
import goto
import subprocess as sp
import webbrowser
"""
    This program "Brian.py" is a basic A.I built to do simple tasks.
    Such as: Google, Github, Voice, etc..
    Author: Karthik Srivijay, Sachin Bhat
    Launguage: Python 2.x
    Version: 2.0.1
"""
# New updates are as follows:-
"""
    1. Screenfetch added
    2. Imported subprocess
    3. Clear added
    
 
"""
#Creating Var(s) for the score.
usrinput=1
exit="exit"
Exit="Exit"
EXIT="EXIT"
myname="Brian"

# askname() is a function which fetches the input for the variable called usrinput.
def askname(usrname):
    usrinput==raw_input("Hey! What's your name? ")
    return usrname

# askuser() is a function which fetches the input for the variable called usrinput.
def askuser(usrinput):
    usrinput=raw_input("Hey! What do you want me to do? ")
    return usrinput

def askuserfixed():
    print "What do you want me to do? "
    usrinput = input()
    return usrinput

# Program starts right now.
tmp = sp.call('clear',shell=True) #Clear Screen
while True: # Making a infinity loop
    usrinput=raw_input("Hey! What do you want me to do? ")
    if usrinput=="exit" or usrinput=="Exit" or usrinput=="Exit":
        print "Good bye! Had a good time with you."
        break
    elif usrinput=="What's the date?" or usrinput=="What is the date today?" or usrinput=="date":
        f = os.popen('date')
        now = f.read()
        print "Today is ", now
    elif usrinput=="Tell me about my computer" :
        os.system('screenfetch')
    elif usrinput=="Change your name" :
        myname=raw_input("Hey! What should i be called as? ") 
    elif usrinput=="Who am i?" :
        os.system('whoami')
    elif usrinput=="Who are you?" :
        print "Awe... I guess something?? Just kidding! I am "+myname
    elif usrinput=="Tell me a joke" :
     os.system('telnet nyancat.dakko.us')
    elif usrinput=="Open Google" :
     webbrowser.open('http://google.co.in', new=2)
    elif usrinput=="Sing a song" :
        os.system("espeak'And I was like baby, baby, baby oh'")
        os.system("espeak 'Like baby, baby, baby no'")
        os.system("espeak'Like baby, baby, baby oh'")
        os.system("espeak 'I thought you'd always be mine (mine)")
        os.system("Baby, baby, baby oh'")
        os.system("espeak 'Like baby, baby, baby no'")
        os.system("espeak 'Like baby, baby, baby ooh'")
        os.system("espeak 'I thought you'd always be mine'")
     
    else:
        print "Invalid Command."
